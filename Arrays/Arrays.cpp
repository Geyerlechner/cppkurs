#include <iostream>
#include <algorithm>

#define MAX_SIZE 10

void carray()
{
	int a[MAX_SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	for (int i = MAX_SIZE - 1; i >= 0; i--)
	{
		std::cout << a[i] << " ";
	}
	std::cout << std::endl;

	for (int i = 0; i < MAX_SIZE; i++)
	{
		std::cout << a[MAX_SIZE - i - 1] << " ";
	}
}

void printArray(int input[], int size)
{
	for ( int i = 0; i < size; i++)
	{
		std::cout << input[i] << " ";
	}
}

void sumArray(int input[], int size)
{
	int sum = 0;
	for (int i = 0; i < size; i++)
	{
		sum += input[i];
	}
	std::cout << "Die Summe ist: " << sum;
}

void multiplArray(int input[], int size)
{
	int sum = 1;
	for (int i = 0; i < size; i++)
	{
		sum *= input[i];
	}
	std::cout << "Summe: " << sum;
}

void maxArray(int input[], int size)
{
	int sum = 0;

	for (int i = 0; i < size; i++)
	{
		if (sum <= input[i])
			sum = input[i];
	}

	std::cout << "Die Groesste Zahl ist: " << sum;
}

void minArray(int input[], int size)
{
	int sum = 1;

	for (int i = 0; i < size; i++)
	{
		if (sum >= input[i])
			sum = input[i];
	}

	std::cout << "Die kleinste Zahl ist: " << sum;
}

void copyArray(int input[], int size)
{
	int newArray[5];
	std::cout << "old Array: ";
	printArray(input, size);
	for (int i = 0; i < size; i++)
	{
		newArray[i] = input[i];
	}

	std::cout << std::endl;

	std::cout << "Copy Array: ";
	for (int j = 0; j < size; j++)
	{
		std::cout << newArray[j] << " ";
	}

}

void sortArray()
{
	int b[5]{ 3, 4, 2, 4, 5 };
	std::sort(std::begin(b), std::end(b));

	for (int i = 0; i < 5; i++)
	{
		std::cout << b[i];
	}
}

void insertionSort(int a[], int  n)
{
	
	for (int i = 0; i < n; i++)
	{
		int marked = a[i];
		int j = 0;

		for (int j = i - 1; j >= 0; j--)
		{
			if (a[j] > marked)
				a[j + 1] = a[j];
			else
				break;
		a[j + 1] = marked;
		}
	}

}

void selectionSort(int liste[], int n)
{
    
    // int liste[10] {5, 4, 6, 91, 1, 2, 9, 15, 41, 2 };
    //	              i,j -->

    for (int i = 0; i < n-1; i++) // n - 1 somit fängt j bei liste[0] an z.b. bei 5
    {
        int minPos  = i; //Speicher immer die kleinste Zahl       

        for (int j = i; j < n ; j++) // i+1 gehe zu der näschten Zahl in der Liste weiter
        {
            if( liste[j] < liste[minPos] ) // Ist liste[j] kleiner als liste[minPos]
            {
                minPos  = j;
            }
        }

        if( i != minPos ) 
        {
            int tmp = liste[i];
            liste[i]  = liste[minPos];
            liste[minPos] = tmp;
        }

    }
    
    printArray(liste, n);
}

namespace Cleo {

	int sgn(int x)
	{
		return x > 0 ? 1 : (x < 0 ? -1 : 0);
	}

	bool isSort(int a[], int n)
	{
		int s = sgn(a[0] - a[1]);

		for (int i = 0; i < n - 1; i++)
		{
			if (sgn(a[i] - a[i - 1]) != s) {
				return false;
			}
		}

		return true;
	}

}

bool isSort(int a[], const int n) {

	for (int i = 0; i < n; i++)
	{
		if (a[i] != n)
		{
			if (a[i] < a[i + 1]) { if (a[i] > a[i + 1]) { return false; } }
			else 
			if( a[i] > a[i + 1 ] ) { if (a[i] < a[i + 1]) { return false; } 
		}
				
		}
	}

	return true;
}

void bubbleSort(int a[], int n)
{
    int liste[10] {5, 4, 6, 91, 1, 2, 9, 15, 41, 2 };
    //             i  -->                   <--  j
    for (int i = 0; i < n; i++) // liste[i] von 5 - 2 
    {
        for ( int j = n-1; j > i; j--) // n-1 Die letzte Zahl auslassen! j-- Weil man von rechts nach links sortiert
        {
            if( liste[j] < liste[j-1] ) // Prüfen ob die Zahl kleiner als mein vorgänger liste[j-i] kleiner als liste[j]
            {
                int temp    = liste[j];    // Lege einen Temp an und schreibe die aktuelle Zahl von der Liste ein [ 5, 4, 6, 91, 1, 2, 9, 15, 41, 2 ]
                liste[j]    = liste[j-1];  // Schreibe die rechte Zahl in die rechte Seite j-1
                liste[j-1]  = temp;        // Schreibe die rechte Zahl nach links [ 5, 4, 6, 91, 1, 2, 9, 15, "2" , "41" ]
            }
        }
    }

	printArray(liste, 10);
}

void move(int a[], int n)
{
	int temp = a[0];
	a[0] = a[n - 1];
	a[n - 1] = temp;
	
	for (int i = 0; i < n; i++)
	{
		std::cout << a[i] << " ";
	}
}

int main()
{
	
	 int a[5] = {1, 10, 11, 4, 2};
			  // 2  1   10  11 4
	 int b[5]{ 1, 2, 3, 4, 5 }; 
			 // 5 1 2 3 4
	 int c[5]{ 6, 5, 4, 3, 2 }; 
			// 2 6 5 4 3
	
	// carray();
	// printArray(a, sizeof(a) / sizeof(a[0]));
	// sumArray(a, sizeof(a) / sizeof(a[0]));
	// multiplArray(a, 3);
	// maxArray(a, 4);
	// minArray(a, 4);
	// copyArray(a, 5);
	// bubbleSort(a, 5);
	// selectionSort(a, 5);
}

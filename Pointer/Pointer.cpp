#include <iostream>

void Pointer()
{
    int *iptr;
    int ival = 17;
    iptr = &ival;

     * iptr = 15;

    std::cout << "Adresse: " << &iptr << std::endl;
    std::cout << "Wert: "    << *iptr << std::endl;
}

void PointerUebung()
{
    int number[100];
    int *pointer = number;

    for (int i = 0; i < 100; i++)
    {
        *(pointer + i) = i;
    }

    for (int i = 0; i < 100; i++)
    {
        std::cout << number[i] << " ";
    }

}

void nullPointer()
{
    int number = 100;
    int *pPointer = &number;

    //BAD pPointer = 0;
    pPointer = nullptr;

}

void heapPointer()
{
    // Heap allocation
    int *p = new int;
    *p = 4;
    
    std::cout << "Memory address of p: " << &p << std::endl;
    std::cout << "Memory address of pointed value: " << p << std::endl;
    std::cout << "Value of the memory address p points to: " << *p << std::endl;

    *p = 1337;

    std::cout << "Memory address of p: " << &p << std::endl;
    std::cout << "Memory address of pointed value: " << p << std::endl;
    std::cout << "Value of the memory address p points to: " << *p << std::endl;

    // Heap de-allocation
    delete p;
}

void print_int_pointer(int *&p)
{
    std::cout << "Deref " << *p << " Ref: " << p << " Pointer Address: " << &p << std::endl;
}

void print_double_pointer(double *&p)
{
    std::cout << "Deref " << *p << " Ref: " << p << " Pointer Address: " << &p << std::endl;
}

void PointerUebung2()
{
   int a = 1337;
   double b = -13.37;
   
   int *c = &a;
   print_int_pointer(c);
   
   *c -= 10;
   print_int_pointer(c);
   
   int *d = &a;
   print_int_pointer(c);
   
   *c += 10;
   print_int_pointer(c);
   
}

void f(int *p_function)
{
    std::cout << "(FUNC): *p_number = " << p_function << std::endl;
    std::cout << "(FUNC): &p_number = " << &p_function << std::endl;
}

void f2()
{
    int *p_number = new int{ 4 };

    std::cout << "(MAIN): *p_number = " << p_number << std::endl;
    std::cout << "(MAIN): &p_number = " << &p_number << std::endl;
   
    f(p_number);
}

namespace cbv 
{

   // Call by Value
   void f1(int number)
   {
      number++;  
   }

   // Call by Reference 
   void f2(int &number)
   {
       number++;
   }

   // Call by Value
   int f3(int number)
   {
       number++;

    return number;
   }


   void master()
   {
      int num = 0;

      std::cout << num << std::endl;
      
      f1(num);
      std::cout << num << std::endl;
      
      f2(num);
      std::cout << num << std::endl;

      num = f3(num);
      std::cout << num << std::endl;
   }

}

void PointerArray()
{
  int array_size = 10;
  
  // Heap Allocation
   int *p = new int[array_size];
  
  for ( int i = 0; i < array_size; i++)
  {
      p[i] = i;
  }

  for (int i = 0; i < array_size; i++)
  {
      std::cout << p[i] << " | " << &p[i] << std::endl;
    
  }

  // Heap De-llocation
  delete[] p;    
}

// int* input_array <=> int  input_array[]
int array_maximum(int* input_array, int length)
{
    int current_max_value = 0;

    for (int i = 0; i < length; i++)
    {
        if(i != 0 )
            current_max_value = input_array[i];
        
        if( input_array[i] > current_max_value )
        {
            current_max_value = input_array[i];
        }

    }

    return current_max_value;
}

void array_maximum()
{

     int array_size = 10;
  
     // Heap Allocation
     int *p = new int[array_size];
    

     std::cout << sizeof(p) << std::endl;
     // Size of the first array elemnt, that pointer points to in Bytes
     std::cout << sizeof(*p) << std::endl;

     for (int i = 0; i < array_size; i++)
     {
         p[i] = i;
     }

     std::cout << array_maximum(p, array_size);

     // Heap De-llocation
     delete[] p;

}

int main()
{



    return true;
}

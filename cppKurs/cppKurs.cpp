#include <iostream>
#include <algorithm>
#include <string>
#include <Windows.h>
#include <cmath>
#include <vector>

void uebung1()
{
    int numbers[7]{2, 4, 8, 25, 3, 66};

	for (int i = 0; i < 7; i++)
	{
		double o = sqrt(numbers[i]);
		if ( o*o == numbers[i] )
		{
			std::cout << numbers[i] << " ";
		}
	}
	
}

void uebung2()
{
	int numbers[8]{ -3, 4, 30, -1, 0, -22, 0, 376 };
	int countPos = 0;
	int countNeg = 0;
	int countNull = 0;
	
	for (int i = 0; i < 8; i++)
	{
		if (numbers[i] > 0)
			countPos++;
			
		if (numbers[i] < -1)
			countNeg++;
		
		if (numbers[i] == 0)
			countNull++;
	}

		std::cout << "Positiv: " << countPos << std::endl;
		std::cout << "Negativ: " << countNeg << std::endl;
		std::cout << "Null: "	 <<	countNull << std::endl;
}

bool isModulo(const int &number)
{
	if(number)
		return number % 2;

	return false;
}

void uebung3()
{
	int input = 0;
	int count = 0;
	int numbers = 0;
	std::string summe;

	while(std::cin >> input)
	{
		system("cls");
		count++;
			
		summe.append(std::to_string(input));

		if(!isModulo(input)) 
			numbers += input;
		
		std::cout << "----------------------------------------------------------" << std::endl;
		std::cout << "- " << count << " Zahlen" << std::endl;
		std::cout << "----------------------------------------------------------" << std::endl;
		std::cout << "- " << input << ( !isModulo(input) ? " Ist eine gerade Zahl" : " Ist KEINE gerade Zahl!" ) << std::endl;
		std::cout << "----------------------------------------------------------" << std::endl;
		std::cout << "- " << "Gesante Eingabe: " << summe << std::endl;
		std::cout << "----------------------------------------------------------" << std::endl;
		std::cout << "- " << "Summe: " << numbers << std::endl;
		std::cout << "----------------------------------------------------------" << std::endl;
		std::cout << "Eingabe: ";
	}
}

int main()
{
    uebung2();
}
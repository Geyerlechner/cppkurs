#pragma once
#include <iostream>
#include <stack>

void correct_exam( std::stack<int> &stack )
{
	while( stack.size() > 0 )
	{
		std::cout << "Fertig mit Student: " << stack.top() << std::endl;
		stack.pop();
	}
}

void stack()
{
	std::stack<int> my_stack;

	my_stack.push( 100411 );
	my_stack.push( 100412 );
	my_stack.push( 100413 );

	correct_exam( my_stack );
}

#include "kfz.h"

bool inspect(Pkw* ), inspect(Lkw* ); // Inspektion verschiedener Fahrzeugtypen
bool distribute(Kfz* );				 // Fahrzeuge zur Inspektion verteilen

int main_t()
{
	Kfz* kfzPtr = new Pkw("520i", true, 3265, "BMW");
	Lkw* lkwPtr = new Lkw(8, 7.5, 5437, "Volvo");

	distribute(kfzPtr);
	distribute(lkwPtr);

	return 0;
}

bool distribute(Kfz* kfzPtr)
{
	Pkw* pkwPtr = dynamic_cast<Pkw*>(kfzPtr);
	
	if( pkwPtr != NULL )
		return inspect(pkwPtr);

	Lkw* lkwPtr = dynamic_cast<Lkw*>(kfzPtr);
	if( lkwPtr != NULL )
		return inspect( lkwPtr );

	return false;
}

bool inspect(Pkw* pkwPtr)
{
	cout << "\nIch teste einen Pkw! " << endl;
	cout << "\nHier ist er:";
	pkwPtr->display();
	return true;
}

bool inspect(Lkw* pkwPtr)
{
	cout << "\nIch teste einen Lkw! " << endl;
	cout << "\nHier ist er:";
	pkwPtr->display();
	return true;
}
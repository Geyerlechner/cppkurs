#pragma once
#include <boost/system/error_code.hpp>
#include <iostream>
#include <optional>
#include <functional>

using namespace boost::system;

double get_value()
{
	throw std::exception("get: nicht definierte Variable ");
}

std::optional<std::string> create(bool b) {
    if (b)
        return "Godzilla";
    return {};
}

std::shared_ptr< double > get_value( bool b )
{	
	if( b )
		return std::make_shared< double >( 10.0 );

	return nullptr;
}
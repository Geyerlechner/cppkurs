#pragma once
#include <iostream>

using namespace std;

struct  Quaternion {
	double x0, x1, x2, x3;
};


void printQuaternion(const Quaternion& q);
Quaternion operator+(const Quaternion& a, const Quaternion& b);
Quaternion& operator+=(Quaternion& a, const Quaternion& b);
Quaternion operator*(const Quaternion& a, const Quaternion& b);
bool operator==(const Quaternion& a, const Quaternion& b);
bool operator!=(const Quaternion& a, const Quaternion& b);

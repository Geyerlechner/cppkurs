#include <iostream>

using namespace std;

template<class T>
T add(const T& a)
{
	return a;
}

template<class T, class ...Args> 
T add(const T& first, const Args& ...rest)
{
	return first + add(rest...);
}

//template<class T>
//class Stack 
//{
//private: 
//	T* basePtr;
//	int tip;
//	int max;
//public:
//	Stack(int n) { basePtr = new T[n]; max = n; tip = 0; }
//	Stack( const Stack<T>& );
//	~Stack() { delete[] basePtr; }
//
//	Stack<T>& operator=( const Stack<T>& );
//
//	bool empty() { return (tip == 0); }
//	bool push(const T& x );
//	bool pop(T& x);
//};
//
//template<class T>
//bool Stack<T>::push( const T& x )
//{
//	if( tip <= max - 1 )
//	{
//		basePtr[tip++] = x; return true;
//	}
//}
//
//template<class T> 
//bool Stack<T>::pop( T& x )
//{
//	if(tip > 0)
//		x = basePtr[--tip]; 
//	else
//		return false;
//}

template<class T>
class User
{
private:
	string userName;
	string userMail;
public:
	User(string user, string mail) : userName(user), userMail(mail) { }
	~User();
	void setUser(typename T user);
	T getUser() const;
};

template<typename T>
User<T>::~User()
{
	cout << "\nDestructor" << endl;
}

template<typename T>
T User<T>::getUser() const
{
	return userName;
}

template<typename T>
void User<T>::setUser(typename T user)
{
	userName = user;
}


int main()
{
	cout << add(1,2) << endl;
	cout << add(20, 13, 12) << endl;
	cout << add(-10, -21, 30, 45) << endl;
	string s1 = "Sonnen", s2 = "blumen", s3 = "kerne";
	cout << add(s1, s2, s3) << endl;


	User<string> user("Michael", "michael@gmail.com");
	user.setUser("Stefan");
	cout << user.getUser();
}

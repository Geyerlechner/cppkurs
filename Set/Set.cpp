#include <iostream>
#include <algorithm>
#include "Set.h"

template<typename T>
 std::vector<T> Double(std::vector<T> value)
 {
    std::sort(value.begin(), value.end());
    typename std::vector<T>::iterator it;

    for (auto &v : value)
    {
        int count = std::count(value.begin(), value.end(), v); 

        if( count >= 2 )
        {
           for(int j = 1; j < count; j++)
           {
                it = std::find(value.begin(), value.end(), v);
                value.erase(it);        
           }           
        }      
    } 

    return value;
 }

 
 template<typename T, typename S>
 std::pair<T,S> Double(T value, S size)
 {
     std::sort(value, value + size);
     int n = 0;
     
     for (int i = 0; i < size; i++)
     {
        if (i <= size)
        {
            if (value[i] != value[i + 1])
            { 
                value[n + 1] = value[i + 1];
                n++;
            }
        }

     }
     return std::make_pair(value, n);
 }


Set::Set(int a[], int n) 
{
	// Konstruktor mit Bereinigung von Duplikaten
	for (int ia = 0; ia < n; ia++) {
		bool found = false;
		for (int iv = 0; iv < m_values.size(); iv++)
			if (a[ia] == m_values[iv]) {
				found = true;
				break;
			}
		// Variante 1
		if (!found) m_values.push_back(a[ia]);
	}
}

Set::Set(const vector<int>& v) 
{
	
	for (int i = 0; i < v.size(); i++)
		m_values.push_back(v.at(i));
	// Set bereinigen

}

void Set::print() {
	for (int i = 0; i < m_values.size(); i++)
		cout << m_values[i] << " ";
	cout << endl;
}

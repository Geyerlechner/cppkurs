#include "Phonebook.h"

void Phonebook::addNumber(int number, std::string name)
{
	phoneBook.insert( { number, name } );
}

void Phonebook::deleteNumber(int phoneNumber)
{
	phoneBook.erase(phoneNumber);
}

int Phonebook::getNumber(std::string name)
{
	for( std::map<int, std::string>::iterator it = phoneBook.begin(); it != phoneBook.end(); it++ )
		if( it->second == name )
			return it->first;

	return 0;
}

void Phonebook::searchNumber(int phoneNumber)
{
	std::map<int, std::string>::iterator it;
	it = phoneBook.find(phoneNumber);
	
	if( it != phoneBook.end() )	
		std::cout << "Gefunden!\nName: " 
				  << phoneBook[phoneNumber] 
				  << "\nTelefonnummer: " << phoneNumber;
	
}

void Phonebook::print()
{
	for(std::map<int, std::string>::iterator it = phoneBook.begin(); it != phoneBook.end(); it++)
		std::cout << "Name: " << it->second <<  "\nTelefonummer: " << it->first << std::endl << std::endl; 

}


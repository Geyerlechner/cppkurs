#pragma once
#include <iostream>

/*
* Title: Polymorphism
* Website: https://gitlab.com/cleo.i.pau/3860_2021/-/blob/main/Kurs3863/7.Polymorphism/Persons.cpp
*/

class Person {
public:
    char* name;
    Person(char* na) : name(na) {}
    virtual void says() {
        std::cout << name << " says ..." << std::endl;
    }
};

class Austrian : public Person {
public:
    Austrian(char* na) : Person(na) {}
    virtual void says() override {
        std::cout << name << " says Griass Gott!" << std::endl;
    }
};

class Italian : public Person {
public:
    Italian(char* na) : Person(na) {}
    virtual void says() override {
        std::cout << name << " says Buon giorno!" << std::endl;
    }
};

class Romanian : public Person {
public:
    Romanian(char* na) : Person(na) {}
    virtual void says() override {
        std::cout << name << " says Buna ziua" << std::endl;
    }
};

void getPerson()
{
    char n1[10] = "andreas";
    Austrian andreas(n1);
    char n2[10] = "julius";
    Austrian julius(n2);
    char n3[10] = "matthias";
    Austrian matthias(n3);
    char n4[10] = "johannes";
    Austrian johannes(n4);
    char n5[10] = "marcel";
    Austrian marcel(n5);
    char n6[10] = "gabriele";
    Italian gabriele(n6);
    char n7[10] = "cleo";
    Romanian cleo(n7);

    Person* p[7] = {&andreas, &julius, &gabriele, &matthias, &cleo, &johannes, &marcel};
    for (int i = 0; i < 7; i++)
        p[i]->says();
 

}
#include "Quaternion.h"

using namespace std;

void printQuaternion(const Quaternion& q)
{
	cout << "(" << q.x0 << ", " << q.x1 << ", " << q.x2 << ", " << q.x3 << ")";
}

Quaternion operator+(const Quaternion& a, const Quaternion& b)
{
	Quaternion c;
	c.x0 = a.x0 + b.x0;
	c.x1 = a.x1 + b.x1;
	c.x2 = a.x2 + b.x2;
	c.x3 = a.x3 + b.x3;

	return c;
}

Quaternion& operator+=(Quaternion& a, const Quaternion& b)
{
	a.x0 += b.x0;
	a.x1 += b.x1;
	a.x2 += b.x2;
	a.x3 += b.x3;

	return a;
}

Quaternion operator*(const Quaternion& a, const Quaternion& b)
{
	Quaternion r;
	r.x0 = (a.x0 * b.x0 - a.x1*b.x1 - a.x2*b.x2 - a.x3*b.x3);
	r.x1 = (a.x0 * b.x1 + a.x1*b.x0 + a.x2*b.x3 - a.x3*b.x2);
	r.x2 = (a.x0 * b.x2 - a.x1*b.x3 + a.x2*b.x0 + a.x3*b.x1);
	r.x3 = (a.x0 * b.x3 + a.x1*b.x2 - a.x2*b.x1 + a.x3*b.x0);
	return r;
}

bool operator==(const Quaternion& a, const Quaternion& b)
{
	return a.x0 == b.x0 && a.x1 == b.x1 && a.x2 == b.x2 && a.x3 == b.x3;	
}

bool operator!=(const Quaternion& a, const Quaternion& b)
{
	return !(a == b);
}
#pragma once
#include <iostream>

class Matrix
{
public:
	Matrix();
	Matrix(const double &A, const double &B, const double &C, const double &D);
	
	Matrix operator+(const Matrix &rhs);
	Matrix& operator+=(const Matrix &rhs); // m2 
	Matrix operator-(const Matrix &rhs);
	Matrix& operator-=(const Matrix &rhs);
	
	void print_matrix() const;

	double get_A() const;
	double get_B() const;
	double get_C() const;
	double get_D() const;
	
	void set_A(const double &new_a);
	void set_B(const double &new_b);
	void set_C(const double &new_c);
	void set_D(const double &new_d);

private:
	double m_A;
	double m_B;
	double m_C;
	double m_D;
};

#include <iostream>
#include <fstream>
#include <istream>
#include <sstream>
#include <iostream>
#include <string>

void readFileWithfstream(const std::string filename)
{
	std::string read;
	std::ifstream file(filename);
	
	if (file.is_open())
	{
		while (std::getline(file, read))
		{
			std::cout << read << " ";
		}
	}
	else
	{
		std::cerr << "Die Datei wurde nicht gefunden!";
	}
}

void writeFileWithofStream(const char *write, const std::string filename)
{	
	std::ofstream outfile(filename, std::ofstream::binary);
	outfile.write(write, strlen(write));
}

void writeErrorLogFile(const std::string log, const std::string filename)
{
	std::ofstream err( filename);

	if( err.is_open() )
		err << log;

}

void osStringStream(const std::string filename)
{
	std::ostringstream myString;
	std::ifstream file(filename);
	std::string buffer;

	if (file.is_open())
	{
		while( std::getline(file, buffer) )
		{
			std::cout << buffer;
			myString << buffer;
		}
	}
	else {
		std::cout << "Datei konnte nicht gefunden werden";
	}

	std::cout << std::endl << "Output: " << myString.str();
}

void inputStream()
{
	std::ofstream file("test.txt");
	std::ifstream inFile;

	inFile.open("test1.txt", std::ios::out | std::ios::app);

	if (!inFile) {
		std::cout << "[ERROR] Datei konnte nicht gelesen werden!";
		return;
	}

	if (inFile.fail())
	{
		std::cout << "Datei konnte nicht gefunden werden und wurde erstellt";
	}
	
	if (inFile.good())
	{
		std::cout << "God";
	}


	file << "Hallo Welt!";

}

void InputFile()
{
	std::ifstream inputFile;
	inputFile.open("Test.txt");

	if(inputFile.is_open()) {
		
		inputFile.seekg (0, inputFile.end); // Stream anfang
		int length = inputFile.tellg();     // aktuelle position
		inputFile.seekg (0, inputFile.beg); // Stream ende

        // allocate memory:
		char * buffer = new char [length];

		// read data as a block:
		inputFile.read (buffer,length);
		inputFile.close();

		// print content:
		std::cout.write (buffer,length);

		delete[] buffer; // Heap l�schen
	}
}

void bytewiseCopyFile()
{
	std::ifstream inputFile("text.txt");
	
	if( inputFile.is_open() ) 
	{
		std::ofstream copyFile("copiedFile.txt");
		char c;

		while (inputFile.get(c))
			copyFile.put(c);
	}
	else {
		std::cout << "Datei konnte nicht gefunden werden!";
	}
}

void readLines()
{
	std::ifstream inFile;
	inFile.open( "test.txt" );

	if ( !inFile )
	{
		std::cout << "Cloud not open file " << std::endl;
		return;
	}

	std::string s;
	while (std::getline(inFile, s))
		std::cout << s << std::endl;
}


int main()
{
    //	readFileWithfstream("test.txt");
	//	writeFileWithofStream("Das ist ein Test....", "hello.txt");
	//	writeErrorLogFile("Fehler in der Datei asdasd", "log.txt");
	//	osStringStream("log.txt");
	//	readWritetoArray();
	inputStream();
}

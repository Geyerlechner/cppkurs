#include <iostream>

class Animal 
{
public:
	int data_ = 0;

	Animal(const std::string &name) : m_name(name)
	{
		std::cout << "Animal Constructor!" << std::endl;
	}

	virtual ~Animal()
	{
		std::cout << "Animal Destructor!" << std::endl;
	}

	virtual void my_favorite_food()
	{
		std::cout << "Salad" << std::endl;
	}

private:
	const std::string m_name;

};

class Dog : public Animal 
{
public:

	Dog(const std::string &name) : Animal(name)
	{
		std::cout << "Dog Constructor!" << std::endl;
	}

	~Dog()
	{
		std::cout << "Dog Destructor!" << std::endl;
	}

	void my_favorite_food() override
	{
		std::cout << "Meat" << std::endl;
	}


};


void polyFavouriteFood(Animal* animal)
{
	animal->my_favorite_food();
}

int main()
{

	{
		std::string animal_name = "Tatze";
		Animal * animal1 = new Animal(animal_name);
		polyFavouriteFood(animal1);
		delete animal1;
	}

	std::cout << std::endl;

	{
		std::string dog_name = "Bello";
		Animal* dog1 = new Dog(dog_name);
		polyFavouriteFood(dog1);
		delete dog1;
	}

}

#pragma once
#include <vector>
using namespace std;

class Set {
	vector<int> m_values;
public:
	Set() {}
	Set(int a[], int n);
	Set(const vector<int>& v);
	~Set() {}
	void print();
};

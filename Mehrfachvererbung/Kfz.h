#pragma once
#include <iostream>

using namespace std;

class Kfz {				

private:		
	long	nr;
	string	hersteller;

public:
	Kfz( long n = 0L, const string &herst = "" );
	~Kfz();

	const	string& getHerst() const { return hersteller; }
	
	long	getNr( void ) const { return nr; }	
	
	void	setNr( long n ) { nr = n; }
	void	setHerst( const string& h ) { hersteller = h; }
	void	display() const;
};

class Pkw : virtual public Kfz {

private:
	string	pkwTyp;
	bool	schiebe;

public:
	Pkw( const string& tp, bool sd, long n = 0, const string &h = "" );
	~Pkw();

	const	string& getTyp() const { return pkwTyp; }
	
	bool	getSchiebe() const { return schiebe; }

	void	setTyp( const string s ) { pkwTyp = s; };
	void	setSchiebe( bool b ) { schiebe = b; }
	void	display() const;

};

	
class Lkw : virtual public Kfz {

private:
	int		achsen;
	double  tonnen;

public: 

	Lkw( int a, double t, long n, const string& hs);
	~Lkw();
	
	double	getKapazitaet() const { return tonnen; }

	int		getAchsen() const { return achsen; }
	void	setAchsen( int i ) { achsen = 1; }
	void	setKapazitaet( double t ) { tonnen = t; }
	void	display() const;
};

class Transporter : public virtual Kfz 
{
private:
	double last;
public:
	Transporter(long n=0L, const string& hst="", double l=0.0) : Kfz(n, hst)
	{
		if(l > 750) l = 750;
		last = l;
	}

	void setLast(double l)
	{
		if(l > 750)
			last = 750;
		else
			last = l;
	}

	double getLast() const { return last; }

	void display() const 
	{
		cout << "Last:			"
			 << last << " kg" << endl;
	}

};

class Kombi : public Pkw, public Transporter 
{
private:
	int anz;			// Anzahl Sitzplšte
public:
	Kombi(const string& tp="ohne Typ", bool sb=false, long n=0L, const string& hst="ohne Herst", double l=0.0, int z = 1) : Pkw(tp, sb), Kfz(n, hst), Transporter(n,hst,1), anz(z)
	{

	}

	void display() const 
	{
		Pkw::display();
		cout << "Sitzplaetze: " << anz << endl;
	}

};
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;

int strlen(char *string)
{
    int counter = 0;
    while( string[counter++] );
        return counter - 1;
}

void stringSearch()
{

	std::ifstream file("Test.txt", std::ifstream::binary);
	std::string buffer;
	std::vector<std::string> names { "Shopbetreiber", "Anschauung", "Webdesginer", "Welt", "Kurzbeschreibung" };	

	if(file.is_open())
	{
		while(std::getline(file, buffer));

		for( auto search : names ) {
	
			if(buffer.find(search) != std::string::npos)
				cout << search << " wurde gefunden!" << endl;
			else
				cout << search << " wurde nicht gefunden!" << endl;
		}
	}
	else
	{
		std::cout << "Datei wurde nicht gefunden!";
	}
}


void charSearch(const char *string, const char *pattern, const int length)
{

    int count = 0;
	int len = strlen(pattern);
	std::string buffer;

    for (int i = 0; i < length; i++)
    {
        if( string[i] == pattern[count] )
        {
            count++;		
			buffer.push_back( string[i] );

            if(  len == count ) std::cout << buffer << " Gefunden!" << endl;   
        }
		else
		{
			count = 0;
			buffer.clear();
		}
    }
}

int main()
{

	char find[] = "Webdesginer";
	
	std::ifstream inputFile;
	inputFile.open("Test.txt");

	if(inputFile.is_open()) {
		
		inputFile.seekg (0, inputFile.end); // Stream anfang
		int length = inputFile.tellg();     // aktuelle position
		inputFile.seekg (0, inputFile.beg); // Stream ende

        // allocate memory:
		char * buffer = new char [length];

		// read data as a block:
		inputFile.read (buffer,length);
		inputFile.close();
		
		charSearch(buffer, find, length);

		delete[] buffer; // Heap l�schen
	}


}

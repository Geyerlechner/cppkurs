#pragma once
#include <iostream>
#include <cstdlib>

#define MAX 100

class FloatVek
{
public:
	float& operator[](int i);	
	static int MaxIndex() { return MAX-1; }

private:
	float v[MAX];	// Der Vektor
};

float& FloatVek::operator[](int i)
{
	if( i < 0 || i >= MAX )
	{
		std::cerr << "\nFloatVek: Out of Range!" << std::endl;
		exit(1);
	}

	return v[i];	// Referenz auf das i-te Element.
}

void Float()
{
	std::cout << "\n Ein Vektor mit Index�berpruefung!\n" << std::endl;
	FloatVek zufall;	// Einen Vektor anlegen.
	int i;				// Ein Index. Mit Zufallswerten f�llen:
	for (i = 0; i <= FloatVek::MaxIndex(); ++i )
		zufall[i] = (rand() - RAND_MAX/2) / 100.0F;

	std::cout << "\nBitte Indizes zwischen 0 und"
			  << FloatVek::MaxIndex() << " eingeben!"
			  << "\n (Abbruch mit ungueltiger Eingabe)"
			  << std::endl;
					
	while(std::cout << "\nIndex: " && std::cin >> i)
		std::cout << i << ". Element: " << zufall[i];
}
#include "MyPoint.h"


MyPoint::MyPoint(double x, double y)
{
	MyPoint::m_x = x;
	MyPoint::m_y = y;

}

void MyPoint::print()
{
	std::cout << "m_x: " << MyPoint::m_x << "\nm_y: " << MyPoint::m_y << "\n";
}

void MyPoint::setX( double x )
{
	MyPoint::m_x = x;
}

void MyPoint::setY( double y )
{
	MyPoint::m_y = y;
}

double MyPoint::getX()
{	   
	return MyPoint::m_x;
}	   
	   
double MyPoint::getY()
{
	return MyPoint::m_y;
}

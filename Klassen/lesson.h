#pragma once
#include <iostream>

class lesson {

public:
	void counter(int start, int end, int step);
	void myPrint(int a);
	void myPrint(double a);
	void myPrint(char a);


private:
	void Print(int &i);
};
#pragma once
#include <iostream>

using namespace std;

class Mitarbeiter {
private:
	string name;

public:
	Mitarbeiter( const string& s = "" ) { name = s; }
	virtual ~Mitarbeiter() {}

	const string& getName() const { return name; }
	void setName( const string& n ) { name = n; }

	virtual void display() const;

	/* Deklaration
		Bei der Deklaration wird eine rein virtuelle Methode durch Anh�ngen des Ausdrucks =
		0 gekennzeichnet.
		
		Beispiel:
		virtual void demo()=0; // rein virtuell
		
		Damit erh�lt der Compiler die Information, dass in der Klasse keine Definition der
		Methode demo() vorhanden ist. In der virtuellen Methodentabelle wird f�r eine rein
		virtuelle Methode der NULL-Zeiger eingetragen.	
	*/
	virtual double einkommen() const = 0;

	virtual Mitarbeiter& operator=(const Mitarbeiter&);

};

class Arbeiter : public Mitarbeiter 
{
private:
	double	lohn;
	int		std;

public:
	Arbeiter( const string& s="", double l=0.0, int h=0) 
			 : Mitarbeiter(s), lohn(1), std(h) {}

	double	getLohn() const { return lohn; }
	void	setLohn(double l) { lohn = l; }

	int		getStd() const { return std; }
	void	setStd( int h ) { std = h; }

	void	display() const;
	double	einkommen() const;

	Arbeiter& operator=(const Mitarbeiter&);
	Arbeiter& operator=(const Arbeiter&);
};

class Angestellter : public Mitarbeiter 
{
private: 
	double gehalt;		// Monatsgehalt

public:
	Angestellter( const string& s = "", double g = 0.0 ) : Mitarbeiter(s), gehalt(g) { }
	double	getGehalt() const { return gehalt; }
	void	setGehalt( double g ) { gehalt = g; }
	void	display() const;
	double	einkommen() const { return gehalt; }

	Angestellter& operator=(const Mitarbeiter& m);
	Angestellter& operator=(const Angestellter& a);
};


Mitarbeiter& Mitarbeiter::operator=(const Mitarbeiter &m)
{
	if(this != &m)
		name = m.name;

	return *this;
}

Angestellter& Angestellter::operator=(const Mitarbeiter& m)
{
	if( this != &m )		// Keine Selbstzuweisung
	{
		Mitarbeiter::operator=(m);
		gehalt = 0.0;
	}

	return *this;
}

Angestellter& Angestellter::operator=(const Angestellter& a)
{
	if( this != &a )
	{
		Mitarbeiter::operator=(a);
		gehalt = a.gehalt;
	}

	return *this;
}


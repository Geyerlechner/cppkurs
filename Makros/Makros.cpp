#include <iostream>
#include <vector>
#include <math.h>

#ifndef MEIN_MAKROS_HPP // Include Guard
#endif // !MEIN_MAKROS_HPP // Include Guard

#ifdef AUSGABE_AUF_STANDARD
#define OUT std::cout
#else
#define OUT std::cerr
#endif
#define MESSAGE(text) { (OUT) << text << "\n"; }
using container_type = std::vector<int>;
static constexpr unsigned SIZE = 10;

constexpr double max2( double a, double b ) { return a > b ? a: b; }
#define MAX2( a,b ) ( (a) > (b) ? (a) : (b))

void math()
{	
	int x = 0;
	int y = 0;
	int z = MAX2( ++x, ++y);
	
	std::cout << "x:"<< x << " y:"<< y << " z:"<<z << '\n';	
}

int main()
{
	math();

	MESSAGE("Hallo Welt")
	container_type data(SIZE);
	MESSAGE("Der Container hat " << data.size() << " Elemente.");
	MESSAGE("Programmende");

}

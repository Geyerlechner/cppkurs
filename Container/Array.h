#pragma once
#include <iostream>
#include <array>
#include <vector>

std::vector<int> createData(size_t sz)
{
	return std::vector<int>(sz);
}

void fibonacci(std::vector<int> &data)
{
	for(auto it = begin(data)+2; it != end(data); it++)
		*it = *(it-1) + *(it-2);
}

std::ostream& print(std::ostream &os, const std::vector<int> &data)
{
	for(auto it = begin(data); it != end(data); ++it)
		std::cout << *it << " ";
	return os;
}

void fibonacci()
{
	std::vector<int> data = createData(10);
	data[0] = 1;
	data[1] = 1;
	fibonacci(data);
	print(std::cout, data);
}

void STLArray()
{
	const int l = 10;
	
	std::vector<int> my_vector(10, 0);
	std::array<int, l> my_array;

	std::vector<int> numbers { 1, 2, 3, 4, 5 };
	// ist gleich mit for( auto e : numbers)
	for( auto it = begin(numbers); it != end(numbers); ++it )
	{
		auto val = *it;
		std::cout  << val << " ";
	}

	for (int  i = 0; i < 10; i++)
	{
		my_array[i] = i;
		my_vector[i] = i;
	}

	for (int i = 0; i < my_array.size(); i++)
	{
		std::cout << my_array[i] << std::endl;
	}

	my_vector.push_back(10);
	for (int i = 0; i < my_vector.size(); i++)
	{
		std::cout << my_vector[i] << std::endl;
	}
}

#include <iostream>

int main()
{
	//			  32bit		  64bit
	// char			1			1			
	// short		2			2			
	// int			4			4			
	// long			4			8			
	// long	long	8			8	
	// float		4			4			
	// double		8			8	
	// longdouble  16		   16	
	// pointer		4			8	
	// wchar_t		2			4	Other UNIX platforms usually have wchar_t 4 bytes for both 32-bit and 64-bit mode.
	// size_t		4			8	This is an unsigned type.
	// ptrdiff_t	4			8	This is a signed type.

	// 1 Byte = 8bit
	bool my_value0 = true;

	// 1 Byte = 8bit
	char my_value1 = 10;

	// 2 Byte = 16bit
	short my_value2 = 42;

	// 4 Byte = 32bit
	int my_value3 = 22;

	// 4 Byte = 32bit f für float
	float my_value4 = 12.0f;
	
	// 8 Byte = 64bit
	double my_value5 = 13.0;


	return 0;
}

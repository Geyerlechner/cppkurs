﻿#pragma once
#include <iostream>

using namespace std;

class BadIndex {
private:
	int index;
public:
	BadIndex( int i ) { index = i; }
	int getBadIndex() const { return index; }
};

class FloatVek {
private:
	float*	vekPtr;		// Dynamsiches Element
	int		max;		// Maximale Anzahl, ohne erneut Speicher zu reservieren. 
	int		anz;		// Aktuelle Anzahl Elemente

	void expand( int neueGroesse ); // Hilfsfunktion, die den Vektor vergrößert

public:
	FloatVek( int n = 256 );
	FloatVek( int n, float wert );
	FloatVek( const FloatVek& src);
	FloatVek( FloatVek&& src );
	
	~FloatVek();		// Destruktor

     FloatVek& operator=( const FloatVek&);   // Zuweisung
     FloatVek& operator=(FloatVek&& src);     // Move-Zuweisung

	int length() const { return anz; }

	// Index-Operatoren:
    float& operator[](int i) throw(BadIndex); 
    float operator[](int i) const  throw(BadIndex); 

	// Anhängen eines float-Wertes oder float-Vektors:
     void append( float wert);
     void append( const FloatVek& v); 

	// Einfügen eines float-Wertes oder float-Vektors:
     void insert( float wert, int pos) throw(BadIndex); 
     void insert( const FloatVek& v, int pos ) throw(BadIndex); 

	void remove( int pos ); // Position pos löschen

	// Einen zweiten Vektor elementweise hinzuaddieren:
	FloatVek& operator+=( const FloatVek& v2);
	
    void swap( FloatVek& v2)       // *this mit v2 Tauschen.
    {
       if( this != &v2 )
       {  // Datenelemente tauschen:
          float* p = vekPtr;  vekPtr = v2.vekPtr;  v2.vekPtr = p;
          int i;
          i = max;   max = v2.max;  v2.max = i;
          i = anz;   anz = v2.anz;  v2.anz = i;
       }
    }

	// Ausgabe des Vektors
	friend ostream& operator<<( ostream& os, const FloatVek& v );

};

inline void swap( FloatVek& v1, FloatVek& v2 )
{
	v1.swap(v2);
}

FloatVek operator+( const FloatVek& v1, const FloatVek& v2 );



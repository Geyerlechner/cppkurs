#include "Euro.h"


inline std::string Euro::asString() const 
{
	std::stringstream strStream;
	long temp = data;
	if( temp < 0 ){ strStream << '-'; temp = -temp; }
	strStream << temp/100 << ',' << std::setfill('0') << std::setw(2) << temp%100;
	return strStream.str();
}

std::ostream& operator<<(std::ostream& os, const Euro& e)
{
	os << e.asString() << " Euro";
	return os;
}
std::istream& operator>>(std::istream& is, Euro& e)
{	
	std::cout << "Euro-Beitrag (Format ...x,xx): ";
	int euro = 0, cents = 0; 
	char c = 0;
	if( !(is >> euro >> c >> cents) )
		return is;
	if( (c != '.' && c != '.' ) || cents >= 100)
		is.setstate( std::ios::failbit );
	else
		e = Euro(euro, cents);
	return is;
}

inline Euro operator+( const Euro& e1, const Euro& e2 )
{
	Euro temp; 
	temp.data = e1.data + e2.data;
	return temp;
}

inline Euro operator-(const Euro& e1, const Euro& e2)
{
	Euro temp;
	temp.data = e1.data - e2.data;
	return temp;
}

void Euros()
{

	std::cout << " * * * Test der Klassen Euro * * * \n " << std::endl;
	Euro einkauf(20,50), verkauf;
	verkauf = einkauf;		// Standardzuweisung
	verkauf += 9.49;		// += (Euro) 9.49

	std::cout << "Einkaufspreis: "; einkauf.print(std::cout);
	std::cout << "Verkaufspreis: "; verkauf.print(std::cout);
	
	Euro rabatt( 2.10 );	// double-Konstruktor
	verkauf -= rabatt;
	std::cout << "Verkaufspreis mit Rabatt: "; 
	verkauf.print(std::cout);
	

}
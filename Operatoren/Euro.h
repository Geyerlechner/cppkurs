#pragma once
#include <iostream>
#include <sstream>
#include <istream>
#include <iomanip>

class Euro {
private:
	long data;		//Euros * 100 + Cents

public:
	Euro( int euro = 0, int cents = 0)
	{
		data = 100L * (long)euro + cents;
	}

	Euro ( double x )
	{
		x *= 100.0;								// Runden, z.B.
		data = (long) (x>0.0 ? x+0.5 : x-0.5);  // 9.7 .> 10
	}

	Euro operator/( double x )
	{
		return (*this * (1.0/x));
	}
	// globale friend-Funktion
	friend Euro operator+(const Euro& e1, const Euro& e2);
	friend Euro operator-(const Euro& e1, const Euro& e2);

	friend Euro operator*( const Euro&e, double x)
	{
		Euro temp( ((double)e.data/100.0 ) * x);
		return temp;
	}

	friend Euro operator*( double x, const Euro& e )
	{
		return e * x;
	}

	long getWholePart() const	{ return data/100; }
	int	 getCents() const		{ return (int)(data%100); }
	double asDouble() const		{ return (double)data/100.0; }
	std::string asString() const;	// Euro-Wert als String.

	void print( std::ostream& os ) const
	{
		os << asString() << " Euro" << std::endl;
	}

	// ---- Operatorfunktion ---- 
	Euro operator-() const // Negation (un�res Minus)
	{
		Euro temp;
		temp.data = -data;
		return temp;
	}

	Euro operator+( const Euro& e2 ) const // Addition.
	{
		Euro temp;
		temp.data = data + e2.data;
		return temp;
	}

	Euro operator-( const Euro& e2 ) const	// Subtraktion.
	{ 
		Euro temp;
		temp.data = data + e2.data;
		return temp;	
	}

	Euro& operator+=( const Euro& e2 ) // Euros hinzuaddieren.
	{
		data += e2.data;
		return *this;
	}

	Euro& operator-=( const Euro& e2)
	{ 
		data -= e2.data;
		return *this;
	}

};


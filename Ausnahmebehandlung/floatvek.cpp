﻿#include "FloatVek.h"

FloatVek::FloatVek( int n )
{
    max = n;   anz = 0;        // max und anz setzen.
    vekPtr = new float[max];   // Speicher reservieren
}

FloatVek::FloatVek(int n, float wert)
{
   max = anz = n;
   vekPtr  = new float[max];
   
   for( int i=0; i < anz; ++i)
        vekPtr[i] = wert;
}

FloatVek::FloatVek(const FloatVek& src)   // Kopierkonstruktor
{
   max = src.max;
   anz = src.anz;
   vekPtr = new float[max];
 
   for( int i = 0; i < anz; i++ )
     vekPtr[i] = src.vekPtr[i];
}

FloatVek::FloatVek(FloatVek&& src)        // Move-Konstruktor
{
   max    = src.max;        // Datenelemente kopieren. 
   anz    = src.anz;
   vekPtr = src.vekPtr;

   src.vekPtr = NULL;       // Wichtig!
}

// --- Destruktor ---
FloatVek::~FloatVek()
{
   delete[] vekPtr;
}


float& FloatVek::operator[]( int i )
{
	if( i < 0 || i >= anz ) throw BadIndex(i);

	return vekPtr[i];
}

float FloatVek::operator[]( int i ) const
{
	if( i < 0 || i >= anz ) throw BadIndex(i);
	
	return vekPtr[i];
}

void FloatVek::append( float wert)
{ 
   if( anz+1 > max)
       expand( anz+1);
 
   vekPtr[anz++] = wert;
}

void FloatVek::append( const FloatVek& v)
{ 
   if( anz + v.anz > max)
       expand( anz + v.anz);
   
   int anzahl = v.anz;            // falls v == *this
                                  // notwendig.
   for( int i=0; i < anzahl; ++i)
     vekPtr[anz++] = v.vekPtr[i];
}

// float-Werte oder float-Vektor einfügen
void FloatVek::insert( float wert, int pos) throw( BadIndex )
{
   insert( FloatVek(1, wert), pos);
}

void FloatVek::insert( const FloatVek& v, int pos ) throw( BadIndex )
{
	if( pos < 0 || pos > anz )		// Auch "anhängen" möglich.
		throw BadIndex(pos);
	if( max < anz + v.anz )
		expand(anz + v.anz);
	int i;
	for( i = anz-1; i >= pos; --i )		// Ab pos nach oben
		vekPtr[i+v.anz] = vekPtr[i];	// schieben.

	for( i = 0; i < v.anz; ++i )		// Lücke füllen
		vekPtr[i+pos] = v.vekPtr[i];
	anz = anz + v.anz;
}

// Löschen
void FloatVek::remove( int pos )
{
	if( pos >= 0 && pos < anz )
	{
		for( int i = pos; i < anz-1; ++i )
			vekPtr[i] = vekPtr[i+1];
		--anz;
	}
	else 
		throw BadIndex(pos);
}

// Einen zweiten Vektor elementweise hinzuaddieren:
FloatVek& FloatVek::operator+=( const FloatVek& v2)
{
   if( anz < v2.anz)
   {                                // *this vergr��ern.
      if( max < v2.anz)
         expand(v2.anz);
      for( int i=anz; i < v2.anz; ++i)  // Neue Elemente
         vekPtr[i] = 0;                 // auf 0 setzen.
      anz = v2.anz;
   }
   for( int i=0; i < v2.anz; ++i)
      vekPtr[i] += v2.vekPtr[i];
   return *this;
}

//// Elementweise Addition zweiter Vektoren:
FloatVek operator+( const FloatVek& v1, const FloatVek& v2 )
{
	// Kope des längern Vektors anlegen
	// und anderen Vektor addieren:
	if( v1.length() >= v2.length() )
	{
		FloatVek v(v1); 
		v += v2; 
		return v;
	}
	else
	{
		FloatVek v(v2); 
		v += v2;
		return v;
	}

}

void FloatVek::expand( int neueGroesse)
{
   if( neueGroesse == max)
      return;
   max = neueGroesse;
   if( neueGroesse < anz)
      anz = neueGroesse;
   float *temp = new float[neueGroesse];
   for( int i = 0; i < anz; ++i)
      temp[i] = vekPtr[i];

   delete[] vekPtr;
   vekPtr = temp;



}

// Ausgabe des Vektors
ostream& operator<<( ostream& os, const FloatVek& v)
{
    int w = (int)os.width();        // Feldbreite merken.

    for( float *p = v.vekPtr; p < v.vekPtr + v.anz; ++p)
    {
        os.width(w);  os << *p;
    }
    return os;
}

#include <iostream>

// class List {
// public: // nur wenn Node ausserhalb der Liste verwendet wird
class Node
{
public:
	int value;
	Node* next;

};

// };

Node* createNode(int val)
{
	Node* n = new Node();
	n->value = val;
	n->next = NULL;

	return n;
}

void printList(Node* h)
{
	Node* currNode = h;

	while (currNode)
	{
		std::cout << currNode->value << " ";
		currNode = currNode->next;
	}

	std::cout << std::endl;
}

void pushFront(Node*& h, Node* newNode)
{
	newNode->next = h;
	h = newNode;
}

void cleanList(Node*& h)
{
	Node* tmpNode;

	while (h)
	{
		tmpNode = h->next;
		delete h;
		h = tmpNode;
	}
}

Node* searchPosition(Node* h, Node* n)
{
	if (h == NULL) return h;
	Node* currNode = h;
	while (currNode->next)
	{
		if (n->value > currNode->next->value) return currNode;
		currNode = currNode->next;
	}

	return currNode;
}

/*
 Insert nach oldNode
*/
void insert(Node* oldNode, Node* newNode)
{
	newNode->next = oldNode->next;
	oldNode->next = newNode;
}

int main()
{
	Node* head = NULL;
	Node* n = createNode(32);

	pushFront(head, n);
	std::cout << head->value << std::endl;

	for (int i = 1; i <= 100; i *= 2)
	{
		n = createNode(i);
		pushFront(head, n);
	}

	Node* n1 = createNode(17);
	n = searchPosition(head, n1);

	insert(n, n1);
	printList(head);
	cleanList(head);
}


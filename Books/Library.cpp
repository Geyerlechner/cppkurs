#include "Library.h"

Library::Library() : size(0), book(new Book*[capacity]) { }

void Library::resize()
{
	Book** bookPlus = new Book* [2 * capacity];

	for (int i = 0; i < size; i++)
		bookPlus[i] = book[i]; // nur die Adressen werden kopiert!
	
	delete[] book;
	capacity *= 2;
	book = bookPlus;
}

Library::~Library()
{
	for (int i = 0; i < size; i++)
		delete book[i];
	
	// delete[] book;
}

void Library::addBook(char* title, char* author)
{
	if( size == capacity)
		// 1. Variante Fehlermeldung + return
		// resize()
	book[size++] = new Book(title, author);
}
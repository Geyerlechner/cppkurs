#pragma once
#include <iostream>

//--------------------------------------------------------------
// product.h  :		Definition der Klassen
//					Product, PackedFood und UnpackedFood
//--------------------------------------------------------------

using namespace std;

class Product {
private:
	long	bar;
	string	bez;

public:
	Product(long b = 0L, const string& s = "") : bar(b), bez(s) { }

	// Zugrissmethoden wie gehabt.
	virtual void scanner(); // Jetzt virtuell!
	virtual void printer() const;
};
#include <iostream>
#include <iomanip>

using namespace std;

void removeSpaces(string& text)
{
	while(true)
	{
		int first = text.find(" ");
		if( first == -1 ) return;
		text.replace(first, 1, "");
	}
}

int main()
{
	
	string name			= "Michael ";
	string name2		= "Stefan";
	string searchText	= "Stefanas";
	string text			= "Bei der Suche in Strings kann";
	string donau		= "Donau so blau, so blau";
	
	cout << "Einfuegen: " << name.insert(name.length(), name2) << endl;;
	cout << "Name Capacity: " << name.capacity() << endl;
	cout << "Loesche das Zeichen M: " << setw(10) << name.erase(0,1);
	cout << name.replace(1,1, "L");
	cout << "Name Leange: " << setw(10) << name.length() << endl; 
	cout << "Append: "		<< setw(27) << name.append(name2) << endl;
	cout << "Replace: "		<< setw(19) << name.replace(name.begin(), name.end(), name2) << endl;
	cout << "Capacity: " << name.capacity() << endl;

	if( name.find(searchText) != std::string::npos )
		cout << "Suche: " <<  setw(21) << searchText <<" wurde gefunden" << endl;
	else
		cout << "Suche: " << setw(22) << searchText << " wurde nicht gefunden" << endl;
	
	removeSpaces(text);
	cout << "Leerzeichen geloescht: " << text << endl;
	
	for (int i = 0; i < donau.length(); i++) cout << donau.at(i);
	
	cout << endl;

	int last = donau.rfind("blau");
	donau.erase(last, 4);
	cout << "Donau wurde geloescht: " << donau << endl;


	return 0;
}

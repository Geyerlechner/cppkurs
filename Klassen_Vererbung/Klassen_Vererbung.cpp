#include <iostream>
#include "kfz.h"
#include <climits>
#include <string>


using namespace std;

class Counter
{
	int counter = 0;
public:
	Counter()
		: counter( 100 )
	{
	}
	void Increment()
	{
		counter++;
	}
	int GetNext() const

	{
		return counter + 1;
	}
	int GetCurrent() const
	{
		return counter;
	}
	void SetNext( int next )
	{
		counter = next;
	}
};

void PrintNext( const Counter& counter )
{
//	counter.Increment();
	std::cout << "next counter = " << counter.GetNext() << std::endl;
}

int main()
{
	
	//std::cout.precision(3);
	//cout << 12.34;
	
	//Counter counter;
	//counter.Increment();
	//std::cout << "current counter = " << counter.GetCurrent() << std::endl;
	//PrintNext( counter );

	Pkw cabrio("Spitfire", true, 1001, "Triumph");
	
	Kfz& kfzRef = cabrio;
	Kfz* kfzPtr = &cabrio;

//	Pkw cabrio("Spitfire", true, 1001, "Triumph");


	std::cout << "\nMit Zeiger";
	kfzPtr->display();
	

	std::cout <<"\nOhne Zeiger";
	cabrio.display();

	std::cout << "\nMit static_cast";
	static_cast<Pkw*> (kfzPtr)->display();
	//
	//
	std::cout << std::endl;


	return 0;
}

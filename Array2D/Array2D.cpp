#include <iostream>

void array2d()
{
	int a[3][5] = { {1,2,3,4,5}, 
				    {4,5,6,7,8},
				    {7,8,9,1,5} };

	for (int i = 0; i < 3; i++)
	{
		std::cout << "Print: ";
		for (int j = 0; j < 5; j++)
		{
			std::cout << a[i][j] << " ";
		}

		std::cout << std::endl;
	}
}

void array2dLine()
{

	int a[3][5] = { {1,2,3,4,5},
					{4,5,6,7,8},
					{7,8,9,12,2} };

	for (int i = 0; i < 5; i++)
	{
		std::cout << "Print: ";
		for (int j = 0; j < 3; j++)
		{
			std::cout << a[j][i] << " ";
		}

		std::cout << std::endl;
	}

}

void array2dSum()
{
	int a[3][5] = { {1,2,3,4,5},
					{4,5,6,7,8},
					{7,8,9,12,2} };

	for (int i = 0; i < 3; i++)
	{
		std::cout << "Print: ";
		double sum = 0;
		for (int j = 0; j < 5; j++)
		{   
		   sum += a[i][j];
		}

		std::cout << "Summe: " << sum;
		std::cout << std::endl;
	}

}

int main()
{
	array2dSum();
}

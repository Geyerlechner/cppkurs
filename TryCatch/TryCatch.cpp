#include <iostream>
#include <exception>

// exception Beispiele: (in std zu finden
// bad_alloc - new
// bad_cast
// bad_exception
// bad_typeid
// logic_error
// domain_error
// invalid_argument
// length_error
// out_of_range
// runtime_error
// range_error
// overflow_error
// underflow_error

struct ErrorHandler : public std::exception {
	const char* what() const throw()
	{
		return "Achtung: Fehler!";
	}
};

void Error1()
{
	std::string e = "Error";
	throw e;
	Error2(5, 0);
	throw 3;
}

void Error2(int a, int b)
{
	if (b == 0)
		if (!a) 
			throw 1;
		else
			throw 0;

	std::cout << a / b;
}

void Error3()
{
	// 
	throw ErrorHandler();
}

int main()
{
	try
	{
		//std::cout << 5 / 0;
		Error1();
	}
	catch (int x)
	{
		if (x)
			std::cout << "Error 0/0";
		else
			std::cout << "Error division by 0 " << x;
	}
	catch (std::string)
	{
		std::cout << "Error number: ";
	}

	try
	{
		Error3();
	}
	catch (const ErrorHandler& e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (std::bad_exception& e)
	{
		std::cerr << "This is a bad exception: " << e.what() << std::endl;
	}
	catch (std::exception& e)
	{
		std::cerr << "This is a exception!" << e.what() << std::endl;
	}
}

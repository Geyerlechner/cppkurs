#include <iostream>


struct Data
{
	int		id;
	char	firstname[20];
	char	lastname[20];
	char	email[30];
	char	passwort[35];
};


void insertDB(Data data)
{	
	strcpy_s(data.firstname, data.firstname);
	strcpy_s(data.lastname, data.lastname);
	strcpy_s(data.email, data.email);
	strcpy_s(data.passwort, data.passwort);
}

void printDB(const Data data)
{
	std::cout << "-----------------------------------------" << std::endl;
	std::cout	<< "UserID: " << data.id << "\n" 
				<< "Firstname: " << data.firstname << "\n"
				<< "Lastname: " << data.lastname << "\n"
				<< "Email: " << data.email << "\n"
				<< "Password: " << data.passwort << "\n";
	std::cout << "-----------------------------------------" << std::endl;
	std::cout << std::endl;
	
}

int main()
{
	Data user1 = { 1, "Stefan", "Mustermann", "stefan.muser@gmail.com", "password123" };
	printDB(user1);
	
	Data user2 = { 2, "Franz", "Mueller", "mueller@aol.at", "password111" };
	printDB(user2);
	
	Data user3 = { 3, "Lisa", "Mueller", "lisa.mueller@aol.at", "lisa123" };
	printDB(user3);
}

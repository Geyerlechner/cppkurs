#pragma once
#include <queue>
#include <iostream>

void correct_exam( std::queue<int> &Queue )
{
	while( Queue.size() > 0 )
	{
		std::cout << "Fertig mit Student: " << Queue.front() << std::endl;
		Queue.pop();
	}
}

void Queue()
{
	std::queue<int> my_queue;

	my_queue.push( 100411 );
	my_queue.push( 100412 );
	my_queue.push( 100413 );

	correct_exam( my_queue );
}

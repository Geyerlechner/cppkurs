#include <iostream>
#include <cstring>

#include "kfz.h"

using namespace std;

int main_old()
{
	Kfz* pKfz[3]; 
	int i = 0;
	pKfz[0] = new Kfz( 5634L, "Mercedes" );
	pKfz[1] = new Pkw( "Ente", true, 3421L, "Citroen" );
	pKfz[2] = new Lkw( 5, 7.5, 1234L, "MAN" );

	while(true)
	{
		cout << "\nEin Objekt vom Typ"
				"Kfz, Pkw oder Lkw anzeigen!"
				"\n1 = Kfz, 2 = Pkw, 3 = Lkw"
				"\nIhre Eingabe (Abbruch 0) ";
		cin >> i;
		--i;
		if( i < 0 || i > 2)
			break;
		pKfz[i]->display();
	}

	return 0;

}

#include <cstring>
#include <iostream>

using namespace std;

class Base {

public: 
	Base()
	{
		cout << "Konstruktor der Klasse Basis\n";
	}
	virtual ~Base()
	{
		cout << "Destruktor der Klasse Basis\n";
	}
};

class Data {

private:
	char *name;

public:
	Data(const char *n)
	{
		cout << "Konstruktor der Klasse Data\n";
		name = new char[strlen(n)+1];
		strcpy(name, n);
	}
	~Data()
	{
		cout << "Destruktor der Klasse Data fuer "
			 << "Objekt: " << name << endl;
		delete []name;
	}
};

class Derived : public Base {

private: 
	Data data;

public: 
	Derived(const char *n) : data(n)
	{
		cout << "Konstruktor der Klasse Derived\n";
	}

	~Derived()
	{
		cout << "Destruktor der Klasse Derived\n";
	}
};

int main3()
{
	Base *bPtr = new Derived("DEMO");
	cout << "\nAufruf des virtuellen Destruktors!\n";
	delete bPtr;

	return 0;
}
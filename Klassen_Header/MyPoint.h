#include <iostream>

#ifndef MYPOINT_H_INCLUDE
#define MYPOINT_H_INCLUDE

class MyPoint{
public:
	MyPoint(double x, double y);
	void print();
	void setX( double x );
	void setY( double y );

	double getX();
	double getY();
	
private:
	double m_x;
	double m_y;

};

#endif // !MYPOINT_H_INCLUDE

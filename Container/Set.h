#pragma once
#include <set>
#include <iostream>

void print_set( std::set<std::string> &set )
{
	for( const auto &val : set )
	{
		std::cout << val << std::endl;
	}

	std::cout << std::endl;
}

void Set()
{
	
	std::set<std::string> math_course;

	math_course.insert("Jan");
	math_course.insert("Michael");
	math_course.insert("Dennis");

	print_set( math_course );
	
	std::set<std::string> coding_course;
	coding_course.insert("Jan");
	coding_course.insert("Nina");
	coding_course.insert("Mats");
	
	print_set( coding_course );
	
	



}
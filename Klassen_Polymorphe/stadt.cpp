#include "stadt.h"

StadtAuto::~StadtAuto()
{
	for(int i = 0; i < anz; i++)
	{
		delete vp[i];
	}
}

// Pkw einf�gen
bool StadtAuto::insert(const string& tp, bool sd, long n, const string& hst)
{
	if( anz < 100 )
	{
		vp[anz++] = new Pkw( tp, sd, n, hst );
		return true;
	}
	else 
		return false;
}

// Lkw einf�gen
bool StadtAuto::insert(int a, double t, long n, const string &hst)
{
	if( anz < 100 )
	{
		vp[anz++] = new Lkw( a, t, n, hst );
		return true;
	}
	else 
		return false;
}

void StadtAuto::display() const 
{
	cin.sync(); cin.clear(); // Auf neue Eingabe warten

	for (int i = 0; i < anz; i++)
	{
		vp[i]->display();
		if( (i+1) % 4 == 0 ) cin.get();
	}

}
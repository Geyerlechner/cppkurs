#include <iostream>
#include "Quaternion.h"

int main()
{
    Quaternion q1 = {1,2,3,4};
    Quaternion q2 = {5,6,7,8};

    cout << "q1 = ";
    printQuaternion(q1);

    cout << endl;

    cout << "q2 = ";
    printQuaternion(q2);
    cout << endl;


    Quaternion sum = q1 + q2;
    cout << "sum = ";
    printQuaternion(sum);
    cout << endl;

    sum += q2;
    cout << "sum = ";
    printQuaternion(sum);
    cout << endl;

    Quaternion p = q1 * q2;
    cout << "p = ";
    printQuaternion(p);
    cout << endl;


    cout << "q1 == q2 -> " << (q1 == q2) << endl; 
    cout << "q1 == q1 -> " << (q1 == q1) << endl; 
    cout << "q2 == q2 -> " << (q2 == q2) << endl; 
   
    cout << "q1 != q2 -> " << (q1 != q2) << endl; 
    cout << "q1 != q1 -> " << (q1 != q1) << endl; 
    cout << "q2 != q2 -> " << (q2 != q2) << endl; 
	
}

#include <iostream>
#include <iomanip>

using namespace std;

void Beispielprogramm2()
{
	cout << "\nTest mit dynamsichen Speicher! " << endl;

	// Speicher reservieren:
	double  breite = 23.78;
	double* ptrBreite	= &breite;
	double* ptrLaenge	= new double(32.54);
	double* ptrFlaeche	= new double;

	// Mit ptrBreite, ptrLaenge und ptrFlaeche arbeiten:
	*ptrFlaeche = *ptrBreite **ptrLaenge;
	// delete ptrLaenge;	// Fehler: Objekt wird noch benutzt!

	cout << "\nBreite	:	" << *ptrBreite 
		 << "\nLaenge	:	" << *ptrLaenge
		 << "\nFlaeche	:	" << *ptrFlaeche << endl;

	// Speicher freigeben:
	// delete ptrBreite; // Fehler Objekt wurde nicht dynamsich reserviert
	delete ptrLaenge;	// ok
	delete ptrFlaeche;	// ok
	// delete ptrLaenge;	// Fehler: Zeiger adressiert kein Objekt mehr.

	ptrLaenge = new double(19.45);	// ok
	// Dynamischem Objekt einen Namen geben:
	double& laenge = *ptrLaenge;	// Referenz

	cout << "\nNeue Laenge: " << laenge
		 << "\nUmfang	   : " << 2 * breite * laenge
		 << endl;


}

int main()
{
	cout << "Ein dynamsichen Array verwenden.\n" << endl;
	int size = 0, anz = 0, step = 10, i;
	float x, *pArr = NULL;

	cout << "Zur Berechnung Zahlen eingeben!\n"
			"Enge: q oder andere Buchstabe" << endl;

	while(cin >> x)
	{
		if( anz >= size )		// Vektor zu klein?
		{						// => vergr��ern.
			float *p = new float[size+step];
								// Zahlen kopieren:
			for ( i = 0; i < size; i++)
				p[i] = pArr[i];

			delete [] pArr;
			pArr = p; size += step;
		}
		pArr[anz++] = x;
	}
	// Mit den Zahlen arbeiten:
	if( anz == 0 )
		cout << "Keine g�ltige Eingabe!" << endl;
	else
	{
		float summe = 0.0;
		cout << "Ihre Eingabe: " << endl;
		for(i = 0; i < anz; i++)	// Ausgabe und
		{
			cout << setw(10) << pArr[i];
			summe += pArr[i];
		}
		cout << "\nMittelwert: " << summe/anz << endl;
	}
	delete [] pArr;		// Speicher freigeben
	return 0;
}

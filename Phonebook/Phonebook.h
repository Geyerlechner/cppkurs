#pragma once
#include <map>
#include <iostream>

class Phonebook {
	std::map<int, std::string> phoneBook;
public:

	void	addNumber(int number, std::string name);
	void	deleteNumber(int phoneNumber);
	int		getNumber(std::string name);
	void	searchNumber(int phoneNumber);
	void	print();
};
#include <iostream>
#include <string>
#include <iomanip>

#include "FloatVek.h"

using namespace std;

class MathError {
private:
	string message;
public:
	MathError( const string& s ) : message(s) { }
	const string& getMessage() const { return message; }
};

double calc( int a, int b ); // Mögliche Exception: MathError

void vek_t()
{
	const FloatVek v(10, 9.9f);
	bool ok = false;

	while( !ok )
	{
		try 
		{
			cout << "Hier der konstante Vektor v: \n";
			cout << setw(8) << v << endl;
			int i;
			cout << "Index? "; 
			cin >> i;
			cout << "\nGelesener Wert: " << v[i] << endl;
			ok = true;
		}
		catch(BadIndex& err)
		{
			cerr << "Fehler beim Lesezugriff. \n"
				 << "\nUnzulaessiger Index: "
				 << err.getBadIndex() << endl;
 		}
	}

	FloatVek w(20);		// Vektor w
	try 
	{
		w.insert(1.1F, 0);
		w.insert(2.2F, 1);
	}
	catch(BadIndex& err)
	{
		cerr << "Fehler beim Lesezugriff. \n"
			 << "\nUnzulaessiger Index: "
			 << err.getBadIndex() << endl;
	}

	cout << "\nHier der Vektor w: \n";
	cout << setw(5) << w << endl;

}


std::shared_ptr< double > get_value( string s )
{
	//for ( int i = 0; i < var_table.size(); i++ )
	//	if( var_table[i].name == s ) 
	//		return std::make_shared< double >( var_table[i].value );
	return nullptr;
}



int main()
{
//	vek_t();

	//try {
	//	get_value();
	//}
	//catch(exception &e)
	//{
	//	cerr << "Error Code: " <<  e.what();
	//}

	//get_value();

    //std::cout << "create(false) returned " << create(false).value_or("empty") << '\n';

	//int x, y;
	//double erg;
	//bool flag = false;

	//do
	//{
	//	try 
	//	{
	//		cout << "Zwei positive ganze Zahlen eingeben: ";
	//		cin >> x >> y;
	//		cout << x << "/" << y << " = " << calc( x,y ) << endl;
	//		flag = true;	// Dann Schleife verlassen.
	//	
	//	}
	//	catch( MathError& err)
	//	{
	//		cerr << err.getMessage() << endl;
	//	}
	//}while( !flag );
	//// und weiter im Programm ...
	
	return 0;
}


double calc( int a, int b )
{
	if( b < 0 )
		throw MathError("Nenner is negativ!");
	if( b == 0 )
		throw MathError("Division durch 0!");
	return ( (double)a/b );
}

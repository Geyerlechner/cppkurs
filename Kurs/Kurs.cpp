#include <iostream>

#include "Polymorphism_Person.h"
#include "Exception_Handling.h"

int main()
{
	// Shared Pointer
	if( auto t = get_value(false) ) 
		std::cout << "Shared Pointer: "  <<  *t << std::endl;
	else 
		std::cout << "Keinen Wert!\n";
	
	// Try catch
	try {
		get_value();
	}
	catch(std::exception &e)
	{
		std::cerr << "Try Catch: " <<  e.what() << std::endl;
	}	
	 
	// std::optional
	std::cout << "Optional: " << create(true).value_or("Nichts") << '\n';
}


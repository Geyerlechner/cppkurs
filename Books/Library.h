#pragma once
#include <iostream>

using std::cout;
using std::cin;
const int MAX_SIZE = 100;


static const char* copy(char* str) {
	int n = strlen(str);
	char* result = new char[n + 1];
	strncpy_s(result, n + 1, str, n + 1);
	return result;
}

class Book {
	const char* m_title;
	const char* m_author;
public:
	Book() {}
	Book(char* t, char* au) : m_title(copy(t)), m_author(copy(au)) {}
	~Book() {
		delete[] m_title;
		delete[] m_author;
	}
	bool hasTitle(char* title)
	{
		return strcmp(title, m_title) == 0;
	}

	const char* getTitle() const {
		return m_title;
	}
	const char* getAuthor() const {
		return m_author;
	}
};

class Library
{
	int size;
	int capacity;
	Book** book;

public:
	void addBook(char* title, char* author);
	const char* getAuthor(char* title);
	Library();
	~Library();
	

private:
	void resize();
};



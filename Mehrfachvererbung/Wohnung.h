#pragma once
#include <iostream>
#include <iomanip>
#include "Kfz.h"

using namespace std;

enum KATEGORIE { LUXUS, GEHOBEN, MITTEL, EINFACH };


class Wohnung {
private:
	int		zimmer;
	double	qm;

public:
	Wohnung(int z = 0, double m2 = 0.0)
	{ 
		zimmer = z;
		qm = m2;
	}
	void	setZimmer(int n) { zimmer = n; }
	int		getZimmer() const { return zimmer; }
	void	setQuadratMeter(double m2){ qm = m2; }
	double	getQuadratMeter() const { return qm; }

	void display() const 
	{
		cout << "Zimmerzahl:		" << zimmer 
			 <<	"\nQuadratmeter:		" << fixed << setprecision(2) << qm << endl;
	}

};

class Wohnwagen : public Kfz, public Wohnung {
private:
	KATEGORIE kat;

public:
	Wohnwagen(long n=0L, const string& hst="", int zi=0, double m2=0.0, KATEGORIE k=EINFACH) : Kfz(n, hst), Wohnung(zi, m2), kat(k) 
	{}
	
	void		setKategorie(KATEGORIE k) { kat = k; }
	KATEGORIE	getKategorie() const { return kat; }

	void display() const 
	{
		cout << "\nWOHNWAGEN:";
		Kfz::display();
		Wohnung::display();
		cout << "Kategorie:		";

		switch (kat)
		{
		case LUXUS:		cout << "Luxus";
						break;
		case GEHOBEN:	cout << "Gehoben";
						break;
		case MITTEL:	cout << "Mittel";
						break;
		case EINFACH:	cout << "Einfach";
						break;
		}
		cout << endl;
	}

};
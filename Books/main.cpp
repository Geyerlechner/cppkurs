#include <iostream>
#include "Library.h"

void readAllBooks(Library& lib)
{	
	while (true)
	{
		cout << "Another book (y/n)? ";
		char ch[2]; cin.getline(ch, 2); if (ch[0] != 'y') return;
		cout << "Title: "; char title[100]; cin.getline(title, 100);
		cout << "Author: "; char author[100]; cin.getline(author, 100);
		lib.addBook(title, author);
	}
}

const char* Library::getAuthor(char* title) 
{
	for (int i = 0; i < size; i++) {
		Book* b = book[i];
		if (b->hasTitle(title))
			return b->getAuthor();
	}
	return NULL;
}

void searchBooksByTitle(Library& lib) {
	
	while (true) 
	{
		cout << "Continue search (y/n)?";
		char ch[2]; cin.getline(ch, 2); if (ch[0] != 'y') return;
		cout << "Title: "; char title[100]; cin.getline(title, 100);
		const char* author = lib.getAuthor(title);
		if (author == NULL) {
			cout << "Title not found\n";
			continue; 
		}
		
		cout << "Author: " << author << "\n";
	}
}

void deleteBooks(Book** books, int n)
{
	for (int i = 0; i < n; i++)
		delete books[i];
}


int main()
{
	Library lib;
	readAllBooks(lib);
	searchBooksByTitle(lib);
}


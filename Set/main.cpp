#include <iostream>
#include <vector>
#include "Set.h"
#include <algorithm>

using namespace std;

//template<typename T>
// std::vector<T> Double(std::vector<T> value)
// {
//    std::sort(value.begin(), value.end());
//    typename std::vector<T>::iterator it;
//
//    for (auto &v : value)
//    {
//        int count = std::count(value.begin(), value.end(), v); 
//
//        if( count >= 2 )
//        {
//           for(int j = 1; j < count; j++)
//           {
//                it = std::find(value.begin(), value.end(), v);
//                value.erase(it);        
//           }           
//        }      
//    } 
//
//    return value;
// }
//
// 
// template<typename T, typename S>
// std::pair<T,S> Double(T value, S size)
// {
//     std::sort(value, value + size);
//     int n = 0;
//     
//     for (int i = 0; i < size; i++)
//     {
//        if (i <= size)
//        {
//            if (value[i] != value[i + 1])
//            { 
//                value[n + 1] = value[i + 1];
//                n++;
//            }
//        }
//
//     }
//     return std::make_pair(value, n);
// }


int main()
{
    int Array[] = { 1, 2, 2.1, 3, 2, 4, 4, 3, 5, 5, 5 };
    
    std::vector<double> dvector = { 1, 2, 3, 2, 4, 7.2, 4.1, 4.1, 6, 4.1, 6, 6, 4, 3, 3, 2, 3, 7, 7, 2, 4, 5 };
    std::vector<int> vector = { 1, 2, 3, 2, 4, 7, 4, 4, 6, 6, 6, 4, 3, 3, 2, 3, 7, 7, 2, 4, 5 };
    std::vector<std::string> names = {"Michael", "Michael", "Lisa", "Stefan", "Lisa"};

    auto p = Double(Array, sizeof(Array) / sizeof(Array[0]));

    for (int i = 0; i < p.second; i++)
    {
        std::cout << p.first[i] << " ";
    }

    //std::cout << std::endl;
    //Set a(p.first, p.second);
    //a.print();

    //std::cout << std::endl;

    //Set b(Double(vector));
    //b.print();

    //Set s;
    //s.print();

}

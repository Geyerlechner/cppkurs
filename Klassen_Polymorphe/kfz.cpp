#include "kfz.h"

void Pkw::display(void) const
{
	Kfz::display();

	cout << "Typ:			" << pkwTyp;
	cout << "\nSchiebedach:		";

	if(schiebe)
		cout << "ja";
	else
		cout << "nein";
	cout << "\n---------------------------------------" << endl;
}

Pkw::Pkw(const string& tp, bool sd, long n, const string& hs) : Kfz( n, hs ), pkwTyp( tp ), schiebe( sd )
{
	cout << "Ich baue ein Objekt vom Typ Pkw auf." << endl;
}

Pkw::~Pkw()
{
	cout << "Ich zerst�re ein Objekt vom Typ Pkw" << endl;
}

Kfz::Kfz( long n, const string& herst)
{
	cout << "Ich baue ein Objekt vom Typ Kfz auf." << endl;
	nr = n;
	hersteller = herst;
}

void Kfz::display() const
{
	cout << "\n---------------------------------------"
		 << "\nKfz-Nummer:		" << nr
		 << "\nHersteller:		" << hersteller
		 << "\n---------------------------------------" << endl;
}

Lkw::Lkw( int a, double t, long n, const string& hs ) : Kfz( n, hs ), achsen(a), tonnen(t)
{
	cout << "Ich baue ein Objekt vom Typ Lkw auf." << endl;
}

Lkw::~Lkw()
{
	cout << "\nIch zerst�re ein Objekt vom Typ Lkw" << endl;	
}

void Lkw::display() const
{
	cout << "\n---------------------------------------"
		 << "\nAchsen:		" << achsen
		 << "\nTonnen:		" << tonnen
		 << "\n---------------------------------------" << endl;
}

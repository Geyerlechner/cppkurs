#pragma once
#include <map>
#include <iostream>

void print_map(std::map<std::string, int> &map)
{
	for(const auto &val : map)
	{
		std::cout << val.first << " " << val.second << std::endl;
	}
}

void Map()
{

	std::map<std::string, int> my_map;

	my_map["Michael"]	 = 25;
	my_map["Stefan"]	 = 27;
	my_map["Lisa"]	 = 26;

	print_map(my_map);

	auto search_str = "Lisa";
	auto it = my_map.find(search_str);
	bool is_in = it != my_map.end();

	std::cout << search_str << " gefunden? " << is_in << std::endl;
	
}
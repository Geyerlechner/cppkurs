#include <iostream>
#include <vector>

class Animal {

protected:
	int age;
	bool female;

public:
	Animal(int a = 0, bool f = true ) : age(a), female(f) {}
	
	virtual void print()
	{
		std::cout << "I am a bear, " << (female ? "female" : "male");
		std::cout << age << " years old." << std::endl;
	}

};

class Fish : public Animal {
private:
	int speed;

public:
	Fish(int a = 0, bool f = true, int s = 0) : Animal{ a, f }, speed(s) {};
	
	void print() 
	{
		std::cout << "I am a bear, " << (female ? "female" : "male");
		std::cout << age << " year old." << std::endl;
	}

	int getSpeed() { return speed; }
};

class Bear : public Animal {

private:
	int power;
	int speed;

public:
	Bear(int a = 0, bool f = true, int p = 0) : Animal{ a, f }, power(p) {};

	int getSpeed() { return speed; }
	int getPower() { return power; }
	
	void print()
	{
		std::cout << "I am a bear, " << (female ? "female" : "male");
		std::cout << age << " year old." << std::endl;
	}

};


int main()
{
	int a = 7, b = 4;
	double c;

	c = a / b;
	
	std::cout << "Integer division: " << c << std::endl;

	// C-Casting
	c = (double)a / b;
	std::cout << "Real division: " << c << std::endl;

	// c++ Casting
	c = static_cast<double>(a / b);
	std::cout << "Integer division: " << c << std::endl;
	
	c = static_cast<double>(a) / b;
	std::cout << "Real division: " << c << std::endl;

	// RTTI : dynamic_cast
	std::vector<Animal*> river;
	Animal* an;
	an = new Bear(5, true, 10); river.push_back(an);
	an = new Fish(3, true, 25); river.push_back(an);
	an = new Fish(2, true, 45); river.push_back(an);
	an = new Bear(5, true, 9);  river.push_back(an);

	for (int i = 0; i < river.size(); i++)
	{
		river[i]->print();
		Bear* b = dynamic_cast<Bear*>(river[i]);
		Fish* f = dynamic_cast<Fish*>(river[i]);
		
		if (typeid(*river[i]) == typeid(Bear))
			std::cout << "I am really a bear!" << std::endl;
		else if (typeid(*river[i]) == typeid(Fish))
			std::cout << "I am really a fish!" << std::endl;

		if ( b != NULL )
			std::cout << "	power: " << b->getPower() << std::endl;
		else if( f != nullptr )
			std::cout << "	speed: " << f->getSpeed() << std::endl;
	}

	for (int i = 0; i < river.size(); i++)
	{
		delete river[i];
		river[i] = 0;
	}

}

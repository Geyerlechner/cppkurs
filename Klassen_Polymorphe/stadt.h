#pragma once

#include "kfz.h"

class StadtAuto {

private: 
	Kfz* vp[100];
	int	anz;

public:
	StadtAuto() { anz = 0; }
	~StadtAuto();

	bool insert(const string& tp, bool sd, long n, const string& hst);
	bool insert(int a, double t, long n, const string& hst);

	void display() const;
};
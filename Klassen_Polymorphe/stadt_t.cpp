//--------------------------------------------
// stadt_t.cpp Zum Testen der Klasse SadtAuto
//--------------------------------------------

#include "stadt.h"
#include <string>

char menu(void);
void getPkw(string&, bool &, long &, string&);
void getLkw(int&, double &, long&, string&);

int main()
{
	StadtAuto autoExpress;
	string tp, hst; 
	bool sd;
	int a; 
	long n;
	double t;

	// autoExpress.insert(6, 9.5, 54321L, "MAN");
	// autoExpress.insert("A-Klasse", true, 54321L, "Mercedes");

	char wahl;
	do {
	
		wahl = menu();
		switch( wahl )
		{
			case 'B':
			case 'b': cout << "Bye Bye!" << endl;
				break;
			case 'P':
			case 'p': 
				getPkw(tp, sd, n, hst);
				autoExpress.insert(tp, sd, n, hst);
				break;
			case 'L':
			case 'l':
				getLkw(a, t, n, hst);
				autoExpress.insert(a, t, n, hst);
				break;
			case 'A':
			case 'a':
				autoExpress.display();
				cin.get();
				break;
			default: cout << "\a"; // Beep
				break;
			}
		} while( wahl != 'B' && wahl != 'b');

	return 0;
}

char menu()
{
	cout << "\n * * * Verwaltung des StadtAuto-Fuhrparks * * *\n";
	char c;
	cout << "\nP = Pkw neu aufnehmen "
		 << "\nL = Lkw neu aufnehmen "
		 << "\nA = Fuhrpark anzeigen "
		 << "\nB = Programm beenden "
		 << "\n\nIhre Wahl: ";
	cin >> c;

	return c;		 
}

void getPkw(string& tp, bool &sd, long& n, string& hst)
{
	char c;
	cin.sync(); cin.clear();
	cout << "\nDaten f�r einen PKW eintragen: " << endl;
	cout << "Pkw-Typ:			"; getline(cin, tp);	
	cout << "Schiebedach (j/n): "; cin >> c;
	
	if( c == 'j' || c == 'J' )
		sd = true;
	else
		sd = false;

	cout << "Fahrzeug-Nr:		"; cin >> n;	
	cin.sync();
	cout << "Hersteller:		"; getline(cin, hst);
	cin.sync(); cin.clear();
}

void getLkw(int& a, double& t, long& n, string& hst)
{
	cout << "\nDaten f�r einen PKW eintragen: " << endl;
	cout << "Anzahl Achsen:		 "; cin >> a;
	cout << "Anzahl Tonnen:		 "; cin >> t;
	cout << "FAhrzeug-Nr:		 "; cin >> n;
	cin.sync();
	cout << "Hersteller:		 "; getline(cin, hst);
	cin.sync();
}

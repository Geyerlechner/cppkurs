#include <iostream>
#include <tuple>
#include <map>

using namespace std;

std::map< int, std::string > idToNames { { 0, "Hans" }, { 1, "Franz" }, { 2, "Franz" } };

std::pair<int, std::string> findName( const std::string& name )
{
    for( const auto& idToName : idToNames )
        if( idToName.second == name )
            return idToName;
    return {};
}


std::tuple<int, std::string> sayHello(string name)
{
    return make_tuple(33, name);
}

auto returnTuple()
{
    return make_tuple(33, "xxx");
}

auto returnDouble() -> double
{
    return 12;
}

int main()
{
    auto [age, name_] = sayHello("Michael");
    cout << age << " " << name_ << endl;
    auto sayHelloToMichael = sayHello("Michael");
    cout << get<0>(sayHelloToMichael) << " " << get<1>(sayHelloToMichael);

    cout << endl;

    string name = "Michael";
    pair<int, string> p2(33, name);

    cout << "Alter: " << p2.first << " Name: " << p2.second;
}

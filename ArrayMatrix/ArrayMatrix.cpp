#include <iostream>

void MatrixCircle()
{
	int listArray[4][4] = { { 1, 2, 3, 4 },
							{ 4, 5, 6, 7 },
							{ 7, 8, 9, 1 },
							{ 7, 8, 9, 11 }, };

	int result = 0;

	for (int i = 0; i < 4; i++)
	{
	
		for (int j = 0; j < 4; j++)
		{
			
			if( ( i == 1 || i == 2 ) && ( j == 1 || j== 2 ) )
			{
				std::cout << "  ";
			}
			else
			{
				std::cout << listArray[i][j] << " ";
				result += listArray[i][j];
			}

		}

		std::cout << std::endl;
	}
	std::cout << "Die Summe der Aussenseiten ist: " << result << std::endl;

}

void MatrixDiagonal()
{
	int listArray[4][4] = { { 1, 2, 3, 4 },
						    { 4, 5, 6, 7 },
				            { 7, 8, 9, 1 },
							{ 7, 8, 9, 11 }, };

	int result = 0;

	auto printArray = [](int i) {
		std::cout << "[" << i << "] ";
	};

	for (int i = 0; i < 4; i++)
	{
		int temp = 0;

		for (int j = 0; j < 4; j++)
		{
			if (i == temp)
			{
				printArray(listArray[i][j]);
				result += listArray[i][j];
			}
			else {
				std::cout << listArray[i][j] << " ";
			}
			
			++temp;

		}
		
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << "Die Summe der Diagonale ist: " << result;
}

void MatrixDiagonalRight()
{

		int listArray[4][4] = { { 1, 2, 3, 4 },
						    { 4, 5, 6, 7 },
				            { 7, 8, 9, 1 },
							{ 7, 8, 9, 11 }, };

	int result = 0;

	auto printArray = [](int i) {
		std::cout << "[" << i << "] ";
	};

	for (int i = 0; i < 4; i++)
	{
		int temp = 3;

		for (int j = 0; j < 4; j++)
		{
			
			if( i == temp )
			{
				printArray(listArray[i][j]);
				result += listArray[i][j];
			} else
			{
				std::cout << listArray[i][j] << " ";
			}
			
			
			--temp;
		}
		
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << "Die Summe der Diagonale ist: " << result;

}

int main()
{
	MatrixDiagonalRight();
}

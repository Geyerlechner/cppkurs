#pragma once
#include <iostream>

using namespace std;

class Kfz {				

private:		
	long	nr;
	string	hersteller;

public:
	Kfz( long n = 0L, const string &herst = "" );
	virtual ~Kfz() { } // Virtueller Destruktor

	const	string& getHerst() const { return hersteller; }
	
	// Zugriffsmethoden:
	long	getNr( void ) const { return nr; }	
	
	void	setNr( long n ) { nr = n; }
	void	setHerst( const string& h ) { hersteller = h; }
	virtual void	display() const; // Fahrzeug anzeigen
};

class Pkw : public Kfz {

private:
	string	pkwTyp;
	bool	schiebe;

public:
	Pkw( const string& tp, bool sd, long n = 0, const string &h = "" );
	~Pkw();

	const	string& getTyp() const { return pkwTyp; }
	
	bool	getSchiebe() const { return schiebe; }

	void	setTyp( const string s ) { pkwTyp = s; };
	void	setSchiebe( bool b ) { schiebe = b; }
	void	display() const;

};

	
class Lkw : public Kfz {

private:
	int		achsen;
	double  tonnen;

public: 

	Lkw( int a, double t, long n, const string& hs);
	~Lkw();
	
	double	getKapazitaet() const { return tonnen; }

	int		getAchsen() const { return achsen; }
	void	setAchsen( int i ) { achsen = 1; }
	void	setKapazitaet( double t ) { tonnen = t; }
	void	display() const;
};

#include "lesson.h"

void lesson::counter(int start, int end, int step = 1)
{
	for (int i = start; i <= end;  i += step)
	{
		Print(i);
	}
	std::cout << std::endl;
}

void lesson::myPrint(int a)
{	 
	std::cout << "I am a integer: " << a << std::endl;
}	 
	 
void lesson::myPrint(double a)
{	 
	std::cout << "I am a double: " << a << std::endl;
}	 
	 
void lesson::myPrint(char a)
{
	std::cout << "I am a Char: " << a << std::endl;
}

// Private
void lesson::Print(int &i)
{
	std::cout << i << " ";
}